package pizzas;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

class TestPizzas {

	private BasePizzas base=new BasePizzas();
	
	@BeforeEach
	public void init() {
		base.addPizzaToMenu(base.createSurpriseWhitePizza());
	}
	
	@Test
	void testAjoutPizza() {
		Pizza p=new Pizza("fromages", 10);
		p.ajoutIngredient(new Ingredient("Mozzarelle", true));
		p.ajoutIngredient(new Ingredient("Talegio", true));
		base.addPizzaToMenu(p);

		assertFalse(base.exists(p.getNom()));
	}
	
	@Test
	 void testAjoutIng1() {
		Pizza p=base.getPizzaFromMenu("Surprise blanche");
		System.out.println(p.formattedIngredients());
		var oldSize=p.ingredients().length;
		p.ajoutIngredient(new Ingredient("brocolis", true));
		assertEquals(oldSize+1,p.ingredients().length);
	}

	@Test
	void testEstRouge() {
		Pizza pizza = new Pizza("Pizza Rouge", 10);
		pizza.ajoutIngredient(new Ingredient("sauce tomate", true));
		assertTrue(pizza.estRouge());
	}

	@Test
	void testEstBlanche() {
		Pizza pizza = new Pizza("Pizza Blanche", 12);
		pizza.ajoutIngredient(new Ingredient("crème fraîche", true));
		assertTrue(pizza.estBlanche());
	}

	@Test
	void testEstVegetarienne() {
		Pizza pizza = new Pizza("Pizza Veggie", 15);
		pizza.ajoutIngredient(new Ingredient("tomate", true));
		pizza.ajoutIngredient(new Ingredient("fromage", true));
		assertTrue(pizza.estVegetarienne());
	}

	@Test
	void testAjoutIngredient() {
		Pizza pizza = new Pizza("Pizza Test", 8);
		Ingredient ingredient = new Ingredient("champignon", true);
		pizza.ajoutIngredient(ingredient);
		assertTrue(pizza.formattedIngredients().contains(ingredient.getNom()));
	}

	@Test
	void testEquals() {
		Pizza pizza1 = new Pizza("Pizza A", 10);
		Pizza pizza2 = new Pizza("Pizza A", 10);
		assertEquals(pizza1, pizza2);
	}

	@Test
	void testVeganize() {
		Pizza pizza = new Pizza("Non Veggie Pizza", 18);
		pizza.ajoutIngredient(new Ingredient("viande", false));
		pizza.ajoutIngredient(new Ingredient("fromage", true));
		pizza.veganize();
		assertTrue(pizza.estVegetarienne());
		assertFalse(pizza.formattedIngredients().contains("viande"));
	}

	@Test
	void testFormattedIngredients() {
		Pizza pizza = new Pizza("Pizza Deluxe", 20);
		pizza.ajoutIngredient(new Ingredient("tomate", true));
		pizza.ajoutIngredient(new Ingredient("fromage", true));
		pizza.ajoutIngredient(new Ingredient("champignon", true));
		assertEquals("tomate fromage champignon ", pizza.formattedIngredients());
	}

	@Test
	void testIngredients() {
		Pizza pizza = new Pizza("Pizza Test", 8);
		Ingredient ingredient1 = new Ingredient("tomate", true);
		Ingredient ingredient2 = new Ingredient("fromage", true);
		pizza.ajoutIngredient(ingredient1);
		pizza.ajoutIngredient(ingredient2);
		Ingredient[] ingredients = pizza.ingredients();
		assertEquals(2, ingredients.length);
		assertArrayEquals(new Ingredient[] { ingredient1, ingredient2 }, ingredients);
	}

	@Test
	void testExists() {
		BasePizzas basePizzas = new BasePizzas();
		assertTrue(basePizzas.exists("sauce tomate"));
		assertFalse(basePizzas.exists("ananas"));
	}

	@Test
	void testPizzasWithMissingIngredient() {
		BasePizzas basePizzas = new BasePizzas();
		Pizza pizza1 = new Pizza("Pizza Test 1", 12);
		pizza1.ajoutIngredient(new Ingredient("sauce tomate", true));
		pizza1.ajoutIngredient(new Ingredient("mozzarelle", true));

		Pizza pizza2 = new Pizza("Pizza Test 2", 15);
		pizza2.ajoutIngredient(new Ingredient("creme fraiche", true));
		pizza2.ajoutIngredient(new Ingredient("jambon blanc", false));

		basePizzas.addPizzaToMenu(pizza1);
		basePizzas.addPizzaToMenu(pizza2);

		List<Pizza> pizzasWithMissingIngredient = basePizzas.pizzasWithMissingIngredient();
		assertEquals(5, pizzasWithMissingIngredient.size());
		assertEquals("berk", pizzasWithMissingIngredient.get(0).getNom());
	}

	@Test
	void testEgal() {
		Ingredient ingredient1 = new Ingredient("Oignon", true);
		Ingredient ingredient2 = new Ingredient("Oignon", true);
		Ingredient ingredient3 = new Ingredient("Poivron", true);

		assertTrue(ingredient1.egal(ingredient2));
		assertFalse(ingredient1.egal(ingredient3));
	}

	@Test
	void testEstRougeFalse() {
		// Crée une pizza sans sauce tomate
		Pizza pizza = new Pizza("Pizza Sans Sauce Tomate", 10);
		Ingredient ingredient = new Ingredient("fromage", true);
		pizza.ajoutIngredient(ingredient);

		// Vérifie que la méthode estRouge() retourne false
		assertFalse(pizza.estRouge());
	}

	@Test
	void testEqualsWithNull() {
		// Crée une pizza
		Pizza pizza = new Pizza("Pizza B", 12);

		// Utilise assertThrows pour vérifier que la méthode equals() lance une NullPointerException
		assertThrows(NullPointerException.class, () -> pizza.equals(null));
	}

	@Test
	void testEqualsDifferentClass() {
		// Crée une pizza
		Pizza pizza = new Pizza("Pizza C", 15);

		// Compare avec un objet d'une classe différente, doit retourner false
		assertNotEquals(pizza, new Object());
	}
}
